import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useSimpleSnackbar} from 'contexts/SimpleSnackbar.jsx';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import {aesDecrypt} from 'utils'
import {ethers} from 'ethers';
import {useDefaultAccount,useAccountCrypt,useRemoveAccount} from 'contexts/StorageProvider'
import IconButton from '@material-ui/core/IconButton';
import {useGlobal} from 'contexts/GlobalProvider.js';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useUpdateGlobal} from 'contexts/GlobalProvider.js'
import CloseIcon from '@material-ui/icons/Close';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

const useStyles = makeStyles(theme => ({
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: '#f50057',
    },
    title: {
        marginTop: theme.spacing(1),
        fontSize: 20
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        textAlign: 'center'
    },
    submit: {
        fontSize: 18,
        width: "40%",
        background:'#3FD3AC',
        margin: theme.spacing(0.5),
        marginTop:theme.spacing(2.5),
        '&:hover':{
            backgroundColor:'#3FD3AC',
        },
        '&:active':{
            backgroundColor:'#3FD3AC',
        }
    },
    import: {
        margin: theme.spacing(2),
        color:"#f44336",
        fontSize: 18,
        textDecoration: "none"
    },
    wallet: {
        textAlign: "center",
        fontSize: 18
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: theme.spacing(3)
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(0.5),
        color: theme.palette.grey[500],
    },
    confirmBtn:{
        width:"40%",
    },
    warn:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        backgroundColor:"#ffebeeee",
        color:"#f44336",
        fontSize:"13px",
    },
    buttonWrap:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        width:"100%",
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between"
    },
}));

function DeleteAccount({closeCallback,open}) {
    const classes = useStyles();
    const {wallet,allWallet} = useGlobal()
    const address = useDefaultAccount()
    const crypt = useAccountCrypt(address)
    const updateGlobal = useUpdateGlobal()
    const showSnackbar = useSimpleSnackbar()
    const [password, setPassword] = useState('')
    const removeAccount = useRemoveAccount()

    const updatePassword = e => {
        let _password = e.target.value;
        setPassword(_password)
    };
    
    //返回主界面
    const handleClose = e => {
        e.preventDefault()
        if(closeCallback) {
            closeCallback()
        }
    }

    const onSubmit = e => {
        e.preventDefault();
        try{
            let privateKey = aesDecrypt(crypt,password)
            let defaultWallet = new ethers.Wallet(privateKey)
            if(defaultWallet.address === wallet.address) {
                showSnackbar("默认账户无法删除",'error')
            } else {
                delete allWallet[wallet.address];
                let options = {
                    wallet:defaultWallet,
                    allWallet,
                    tokenSelectedIndex:0,
                    accountName:allWallet[address].accountName,
                }
                updateGlobal(options)
                removeAccount(wallet.address)
                showSnackbar("删除成功",'success')
                closeCallback()
            }
        }catch(err) {
            showSnackbar("密码错误",'error')
        }
    }

    function showPanel(classes) {
        return(
            <div className={classes.container}>
                <Avatar className={classes.avatar}>
                    <DeleteForeverIcon/>
                </Avatar>
                <Typography className={classes.title}>
                    请输入你的账户密码以确认删除
                </Typography>
                <form className={classes.form} onSubmit={onSubmit}>
                    <FormControl margin="normal"  fullWidth>
                        <TextField id="key-password-input"
                            label='账户密码'
                            required
                            autoFocus
                            variant="outlined"
                            type="password"
                            onChange={updatePassword}
                        />
                    </FormControl>
                    <div className={classes.warn}>
                        注意：在删除前请确认您是否已拥有该导入账户的原始账户助记词或账户密钥
                    </div>

                    <div className={classes.buttonWrap}>
                        <Button className={classes.confirmBtn} variant='outlined' onClick={handleClose}>
                            取消
                        </Button>
                        <Button  type='submit' className={classes.confirmBtn} color='primary' variant='outlined' disabled={!password}>
                            确认
                        </Button>
                    </div>
                </form>
            </div>
        )
    }

    return (
        <Dialog
            fullWidth
            maxWidth='xs'
            open={open}
            onClose={handleClose}
            aria-labelledby="account-dialog-title"
            aria-describedby="account-dialog-description"
        >
            <DialogTitle id="customized-dialog-title" >
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            {showPanel(classes)}
        </Dialog>
    )
}

export default DeleteAccount
