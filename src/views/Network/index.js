import React ,{useRef, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import {useSimpleSnackbar} from 'contexts/SimpleSnackbar.jsx';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {useGlobal,useUpdateGlobal} from 'contexts/GlobalProvider.js'
import {validURL} from 'utils'
import {_INTERFACE_ID_TOKEN_BALANCES,_INTERFACE_ID_ERC721_ENUMERABLE,_NETWORKS} from '../../constants'
import {useUpdateStorageByTag} from 'contexts/StorageProvider'

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
        height:500,
        maxHeight:500,
        // overflowY:'scroll',
    },
    confirmBtn:{
        width:"40%",
    },
    buttonWrap:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        width:"100%",
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    form:{
        // maxWidth: '90vw',
        margin: '0px 15px',
        width:'90%',
    },
    iconWrapper:{
        width:"100%",
        textAlign:"center",
        position:"relative",
        // marginBottom:theme.spacing(1)
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(0.5),
        top: '-10px',
        color: theme.palette.grey[500],
    },
}));

const valuesInit = {
    networkName:'',
    rpcUrl:'',
    chainId:'',
    symbol:'',
    blockBrowserUrl:''
}

const minLength = 3

function Network({onClose}) {
    const classes = useStyles();
    const ref = useRef()
    const [values,setValues] = useState(valuesInit)
    const {networks} = useGlobal()
    const updateGlobal = useUpdateGlobal()
    const updateStorageByTag = useUpdateStorageByTag()
    const showSnackbar = useSimpleSnackbar()

    //关闭本页面，回到钱包主界面
    const handleClose = e => {
        e.preventDefault()
        if(onClose) {
            onClose()
        }
    }
    
    const handleChange = name => event => {
        setValues({
            ...values,            
            [name]:event.target.value,
        })
    };

    const onSubmit = e => {
        e.preventDefault();
        const {networkName, rpcUrl, chainId, symbol, blockBrowserUrl} = values
        if (networkName.length < minLength) {
            return showSnackbar("网络名称长度至少${minLength}位", "error");
        }
        if (!validURL(rpcUrl)) {
            return showSnackbar(`RPC地址不正确`, "error");
        }
        
        try{
            const val = {
                id:chainId,
                network:'local',
                name:networkName,
                browser:blockBrowserUrl,
                symbol:symbol,
                rpc:rpcUrl,
            }
            updateStorageByTag(_NETWORKS,Object.keys(networks).length,val)
            updateGlobal({
                networks:{...networks,[Object.keys(networks).length]:val}
            })
            showSnackbar("导入成功",'success')
            setValues ({
                ...valuesInit,
            })
        }catch(err) {
            console.log('Submit Network error: ', err)
            showSnackbar("写入浏览器存储出错",'error')
        }
    }

    const {networkName, rpcUrl, chainId, symbol, blockBrowserUrl} = values

    return(
        <div className={classes.container}>   
            <div ref={ref} className={classes.iconWrapper}>
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </div>
            <Typography variant='h5' align='center' gutterBottom>
                添加网络
            </Typography>
            <form className={classes.form} onSubmit={onSubmit}>
                <FormControl margin="normal"  fullWidth>
                    <TextField id="network-name-input"
                        label='网络名称'
                        required
                        autoFocus
                        variant="outlined"
                        type="text"
                        value={networkName}
                        onChange={handleChange('networkName')}
                    />
                </FormControl>
                <FormControl margin="normal"  fullWidth>
                    <TextField id="rpc-url-input"
                        label='RPC地址'
                        required
                        variant="outlined"
                        type="text"
                        value={rpcUrl}
                        onChange={handleChange('rpcUrl')}
                    />
                </FormControl>
                <FormControl margin="normal"  fullWidth>
                    <TextField id="chain-id-input"
                        label='链ID'
                        required
                        variant="outlined"
                        type="number"
                        value={chainId}
                        onChange={handleChange('chainId')}
                    />
                </FormControl>
                <FormControl margin="normal"  fullWidth>
                    <TextField id="symbol-input"
                        label='符号'
                        variant="outlined"
                        type="text"
                        value={symbol}
                        onChange={handleChange('symbol')}
                    />
                </FormControl>
                <FormControl margin="normal"  fullWidth>
                    <TextField id="block-browser-url-input"
                        label='区块浏览器 URL'
                        variant="outlined"
                        type="text"
                        value={blockBrowserUrl}
                        onChange={handleChange('blockBrowserUrl')}
                    />
                </FormControl>

                <div className={classes.buttonWrap}>
                    <Button className={classes.confirmBtn} variant='outlined' onClick={handleClose}>
                        取消
                    </Button>
                    <Button  type='submit' className={classes.confirmBtn} color='primary' variant='outlined' 
                        disabled={!networkName || !rpcUrl || !chainId}>
                        保存
                    </Button>
                </div>
            </form>
        </div>
    )
}

export default Network
