import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Logo from 'components/Logo'
import MenuBtn from 'components/MenuBtn'
import AccountBtn from 'components/AccountBtn'
import grey from '@material-ui/core/colors/grey';
import {useGlobal} from 'contexts/GlobalProvider';

const useStyles = makeStyles({
    topHeader:{
        backgroundColor:grey[200]
    },
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        minHeight:'60px',
        paddingLeft: '10px',
        paddingRight: '10px',
    }
});

export default function WalletBar() {
    const classes = useStyles();
    const {isLogin} = useGlobal()
    console.log('isLogin========',isLogin)
    return (
        <div className={classes.topHeader}>
            <Toolbar className={classes.container}>
                <Logo/>
                <MenuBtn/>
                {isLogin ? <AccountBtn/> : ''}
            </Toolbar>
        </div>
   );
}
