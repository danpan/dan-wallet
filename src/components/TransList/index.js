import React, {useEffect,useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useGlobal,useUpdateGlobal} from 'contexts/GlobalProvider'
import {useErc20Tokens} from 'contexts/StorageProvider'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ReceiveIcon from '@material-ui/icons/SaveAlt';
import SendIcon from '@material-ui/icons/CallMade';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {shortenAddress,convertHexToString,getErc20TransactionList,isSender} from 'utils';
import TransactionDetail from 'components/TransactionDetail'
import { withRouter } from "react-router";
import dayjs from 'dayjs';


const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        // maxWidth: 380,
        // backgroundColor: theme.palette.background.paper,
        position: 'relative',
        // overflowX: 'hidden',
        // maxHeight: 200,
    },
    listItem:{
        borderBottom: '1px solid #e5e5e5',
        Height: "80px"
    },
    list:{
        paddingTop: '0px',
        paddingBottom: '0px'
    },
    transIcon:{
        "& svg":{
            marginTop: '20px',
            fontSize: '1.8rem',
        },
    },
    transTitle:{
        fontSize: '1.2rem',
    },
    transSubtitle:{
        fontSize: '0.8rem',
    },
}));

function TransList() {
    const classes = useStyles()
    const {network,wallet,tokenSelectedIndex,provider} = useGlobal()
    const {address} = wallet
    const updateGlobal = useUpdateGlobal()
    const tokens = useErc20Tokens(address,network)
    const isToken = tokenSelectedIndex !== 0
    const token = tokens[tokenSelectedIndex-1] || {}
    const [transList,setTransList] = useState([])
    const [hash,setHash] = useState(null)
    token.symbol = isToken ? token.symbol : "ETH"


    //显示交易详情
    const showDetail = (hash) => {
        setHash(hash)
    }
    //关闭交易详情
    const hideDetail = () => {
        setHash(null)
    }

    //每次切换网络或者用户时，更新刷新状态
    useEffect(()=> {        

        if(wallet && network){
            updateTransactionList()
        }

    },[wallet,network,provider,updateGlobal,tokenSelectedIndex])// eslint-disable-line react-hooks/exhaustive-deps

    const updateTransactionList = () => {
        getErc20TransactionList(address,token,network,provider).then(res => {
            setTransList(res?.reverse())
        })
    }

    function TransactionList() {
        return (
            <List className={classes.list}>
                { transList?.map( (transItem,index) => {
                    return (
                        <React.Fragment key={transItem.hash}>
                        <ListItem disableRipple button  className={classes.listItem} onClick={() => showDetail(transItem.hash)}>
                            <Grid container spacing={2}>
                                <Grid item>
                                    <ListItemIcon className={classes.transIcon}>
                                        { isSender(transItem.from, address) ? <SendIcon />:<ReceiveIcon /> }
                                    </ListItemIcon>
                                </Grid>
                                <Grid item xs={12} sm container direction="column">
                                    <Grid item xs={12} sm container direction="row">
                                        <Grid item xs container spacing={2}>
                                            <Grid item xs>
                                                <Typography gutterBottom variant="subtitle1" className={classes.transTitle}>
                                                    { isSender(transItem.from, address) ? "发送":"接收" }
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="subtitle1" className={classes.transTitle}>
                                                { convertHexToString(transItem.value, 'eth', 4) } { token.symbol }
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                    <Grid item xs={12} sm container direction="row">
                                        <Grid item xs>
                                            <Typography variant="body2" gutterBottom className={classes.transSubtitle}>
                                                交易ID：{ shortenAddress(transItem.hash)}
                                            </Typography>
                                            <Typography variant="body2" color="textSecondary" className={classes.transSubtitle}>
                                                { dayjs(transItem?.timestamp * 1000).format('YYYY-MM-DD HH:mm:ss')} • {isSender(transItem.from, address) ? "至：" + shortenAddress(transItem?.to):"从："+shortenAddress(transItem?.from) }
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            {/* { <TransactionDetail open={openDetail} closeCallback={hideDetail} transItem={transItem}/>} */}
                        </ListItem>
                        { (transItem.hash === hash) ? <TransactionDetail  openDetail={(transItem.hash === hash)} onClose={hideDetail} transItem={transItem} /> : null}
                        </React.Fragment>
                    )
                })}
            </List>
        )
    }

    // let transItem = transList.map((transItem,index)=> function(transItem) {
    //     return 
    //     <ListItem button>
    //       <ListItemIcon>
    //         <SendIcon />
    //       </ListItemIcon>
    //       <ListItemText primary={transItem.hash} />
    //     </ListItem>
    // })

    return (
        <div className={classes.root}>
            { TransactionList() }
        </div>
    )
}


export default withRouter(TransList)
