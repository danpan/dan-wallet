import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useSimpleSnackbar} from 'contexts/SimpleSnackbar.jsx';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import {aesEncrypt} from 'utils';
import {ethers} from 'ethers';
import {useUpdateCrypt} from 'contexts/StorageProvider'
import IconButton from '@material-ui/core/IconButton';
import {useGlobal} from 'contexts/GlobalProvider.js';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import {useUpdateGlobal} from 'contexts/GlobalProvider.js'
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: '#3FD3AC',
    },
    title: {
        marginTop: theme.spacing(1),
        fontSize: 20
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        textAlign: 'center'
    },
    submit: {
        fontSize: 18,
        width: "40%",
        background:'#3FD3AC',
        margin: theme.spacing(0.5),
        marginTop:theme.spacing(2.5),
        '&:hover':{
            backgroundColor:'#3FD3AC',
        },
        '&:active':{
            backgroundColor:'#3FD3AC',
        }
    },
    import: {
        margin: theme.spacing(2),
        color:"#f44336",
        fontSize: 18,
        textDecoration: "none"
    },
    wallet: {
        textAlign: "center",
        fontSize: 18
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: theme.spacing(3)
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(0.5),
        color: theme.palette.grey[500],
    },
    confirmBtn:{
        width:"40%",
    },
    warn:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        backgroundColor:"#ffebeeee",
        color:"#f44336",
        fontSize:"13px",
    },
    buttonWrap:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        width:"100%",
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between"
    },
}));

function CreateAccount({closeCallback,open}) {
    const classes = useStyles();
    const {password,allWallet} = useGlobal()
    const updateGlobal = useUpdateGlobal()
    const showSnackbar = useSimpleSnackbar()
    const updateCrypt = useUpdateCrypt()
    const [accountName,setAccountName] = useState('')

    const handleChange = e => {
        setAccountName(e.target.value)
    }
    
    //返回主界面
    const handleClose = e => {
        e.preventDefault()
        if(closeCallback) {
            closeCallback()
        }
    }

    const onSubmit = e => {
        e.preventDefault()
        let wallet = null;

        try {
            wallet = ethers.Wallet.createRandom();
        }catch(err) {
            showSnackbar("当前浏览器不支持创建随机钱包",'error')
        }
        if(wallet) {
            try{
                let _crypted = aesEncrypt(wallet.privateKey,password);
                let isImported = false
                updateCrypt(wallet.address,_crypted,accountName,isImported)
                wallet['accountName'] = accountName
                wallet['isImported'] = isImported
                allWallet[wallet.address] = wallet

                updateGlobal({
                    isLogin:true,
                    password,
                    wallet,
                    accountName,
                    allWallet,
                })
                showSnackbar("创建成功",'success')
                closeCallback()
            }catch(err) {
                showSnackbar("写入浏览器存储出错",'error')
            }
        }

    }

    function showPanel(classes) {
        return(
            <div className={classes.container}>
                <Avatar className={classes.avatar}>
                    <PersonAdd/>
                </Avatar>
                <Typography className={classes.title}>
                    请输入你的账户名称
                </Typography>
                <form className={classes.form} onSubmit={onSubmit}>
                    <FormControl margin="normal"  fullWidth>
                        <TextField id="key-password-input"
                            label='账户名称'
                            required
                            autoFocus
                            variant="outlined"
                            type="text"
                            value={accountName}
                            onChange={handleChange}
                        />
                    </FormControl>

                    <div className={classes.buttonWrap}>
                        <Button className={classes.confirmBtn} variant='outlined' onClick={handleClose}>
                            取消
                        </Button>
                        <Button  type='submit' className={classes.confirmBtn} color='primary' variant='outlined' disabled={!accountName}>
                            确认
                        </Button>
                    </div>
                </form>
            </div>
        )
    }

    return (
        <Dialog
            fullWidth
            maxWidth='xs'
            open={open}
            onClose={handleClose}
            aria-labelledby="account-dialog-title"
            aria-describedby="account-dialog-description"
        >
            <DialogTitle id="customized-dialog-title" >
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            {showPanel(classes)}
        </Dialog>
    )
}

export default CreateAccount
