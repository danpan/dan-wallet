import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useSimpleSnackbar} from 'contexts/SimpleSnackbar.jsx';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import HowToReg from '@material-ui/icons/HowToReg';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import Select from '@material-ui/core/Select';
import {aesEncrypt} from 'utils';
import {ethers} from 'ethers';
import {useUpdateCrypt} from 'contexts/StorageProvider'
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import {Visible,Invisible} from 'components/Eyes';
import {useGlobal,useUpdateGlobal} from 'contexts/GlobalProvider.js';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles(theme => ({
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: '#3FD3AC',//theme.palette.secondary.main
    },
    title: {
        marginTop: theme.spacing(1),
        fontSize: 20
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        textAlign: 'center'
    },
    submit: {
        fontSize: 18,
        width: "40%",
        background:'#3FD3AC',
        margin: theme.spacing(0.5),
        marginTop:theme.spacing(2.5),
        '&:hover':{
            backgroundColor:'#3FD3AC',
        },
        '&:active':{
            backgroundColor:'#3FD3AC',
        }
    },
    import: {
        margin: theme.spacing(2),
        color:"#f44336",
        fontSize: 18,
        textDecoration: "none"
    },
    wallet: {
        textAlign: "center",
        fontSize: 18
    },
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: theme.spacing(3)
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(0.5),
        color: theme.palette.grey[500],
    },
    confirmBtn:{
        width:"40%",
    },
    warn:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        backgroundColor:"#ffebeeee",
        color:"#f44336",
        fontSize:"13px",
    },
    buttonWrap:{
        marginTop:theme.spacing(2),
        marginBottom:theme.spacing(2),
        width:"100%",
        display:"flex",
        flexDirection:"row",
        justifyContent:"space-between"
    },
}));

const valuesInit = {
    accountName:'',
    showKey:false,
    isPrivateKey:true,
    key:''
}

function ImportAccount({closeCallback,open}) {
    const classes = useStyles();
    const {password,allWallet} = useGlobal()
    const [values,setValues] = useState(valuesInit)
    const showSnackbar = useSimpleSnackbar()
    const updateCrypt = useUpdateCrypt()
    const updateGlobal = useUpdateGlobal()

    const changeKeyType = e => {
        e.preventDefault()
        setValues ({
            ...valuesInit,
            isPrivateKey:!values.isPrivateKey,
        })        
    };

    const handleChange = name => event => {
        setValues({
            ...values,            
            [name]:event.target.value,
        })
    };
    
    const handleChangeShow = name => () => {
        setValues({
            ...values,
            [name]:!values[name]
        })
    };
    
    //返回主界面
    const handleClose = e => {
        e.preventDefault()
        if(closeCallback) {
            closeCallback()
        }
    }

    const handleMouseDownPassword = e => {
        e.preventDefault();
    };

    const onSubmit = e => {
        e.preventDefault()
        let wallet;
        if(isPrivateKey) {
            try{
                wallet = new ethers.Wallet(key);
            }catch(err){
                return showSnackbar("无效的私钥", "error");
            }
        }else{
            try{
                wallet = ethers.Wallet.fromMnemonic(key);
            }catch(err){
                return showSnackbar("无效的助记词", "error");
            }
        }
        if(wallet) {
            try{
                let _crypted = aesEncrypt(wallet.privateKey,password);
                let isImported = true
                updateCrypt(wallet.address,_crypted,accountName,isImported)
                wallet['accountName'] = accountName
                wallet['isImported'] = isImported
                allWallet[wallet.address] = wallet
                updateGlobal({
                    wallet,
                    allWallet,
                    accountName,
                })
                console.log('allWallet=====',allWallet)
                // history.push('/detail')
                showSnackbar("导入成功",'success')
                closeCallback()
            }catch(err) {
                showSnackbar("写入浏览器存储出错",'error')
            }
        }

    }

    const {accountName,showKey,isPrivateKey,key} = values;
    function showPanel(classes) {
        return(
            <div className={classes.container}>
                <Avatar className={classes.avatar}>
                    <HowToReg/>
                </Avatar>
                <Typography className={classes.title}>
                    { isPrivateKey ? "请输入你的私钥": "请输入你的助记词" }
                </Typography>
                <form className={classes.form} onSubmit={onSubmit}>
                    <FormControl margin="normal"  fullWidth>
                        <TextField id="account-name-input"
                            label='账户名称'
                            required
                            autoFocus
                            variant="outlined"
                            type="text"
                            value={accountName}
                            onChange={handleChange('accountName')}
                            inputProps={{
                                name: 'type',
                                id: 'account-name-input',
                            }}
                        />
                    </FormControl>
                    <FormControl margin="normal"  fullWidth variant="outlined">
                        <InputLabel htmlFor="outlined-type-native-simple">请选择导入类型</InputLabel>
                        <Select
                            native
                            label="请选择导入类型"
                            onChange={changeKeyType}
                            inputProps={{
                                name: 'type',
                                id: 'outlined-type-native-simple',
                            }}
                        >
                            <option value={true}>私钥</option>
                            <option value={false}>助记词</option>
                        </Select>
                    </FormControl>
                    <FormControl margin="normal"  fullWidth>
                        <TextField id="key-password-input"
                            label={isPrivateKey ? "私钥" : "助记词"}
                            required
                            autoFocus
                            variant="outlined"
                            type={showKey ? "text" : "password"}
                            onChange={handleChange('key')}
                            InputProps={{
                                endAdornment:(
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle key visibility"
                                            onClick={handleChangeShow('showKey').bind()}
                                            onMouseDown={handleMouseDownPassword}
                                        >
                                            {showKey ? <Visible /> : <Invisible />}
                                        </IconButton>
                                    </InputAdornment>
                                )
                            }}
                        />
                    </FormControl>
                    
                    <div className={classes.buttonWrap}>
                        <Button className={classes.confirmBtn} variant='outlined' onClick={handleClose}>
                            取消
                        </Button>
                        <Button  type='submit' className={classes.confirmBtn} color='primary' variant='outlined' disabled={!key} >
                            导入
                        </Button>
                    </div>
                    {/* <Button type='submit' variant="contained" color="primary" className={classes.submit}>
                        导入
                    </Button> */}
                </form>
                {/* <Button variant="contained" color="primary" onClick={handleClose} className={classes.submit}>
                    取消
                </Button> */}
            </div>
        )
    }

    return (
        <Dialog
            fullWidth
            maxWidth='xs'
            open={open}
            onClose={handleClose}
            aria-labelledby="account-dialog-title"
            aria-describedby="account-dialog-description"
        >
            <DialogTitle id="customized-dialog-title" >
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            {showPanel(classes)}
        </Dialog>
    )
}

export default ImportAccount
