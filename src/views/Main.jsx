import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import WalletBar from 'components/WalletBar';
import { isMobile } from 'react-device-detect';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Routes from 'layouts/Routes';
import SignTransaction from './SignTransaction'
import { getPathBase } from 'utils'

const HEIGHT = "100%"
const MAXWIDTH = 400

const useStyles = makeStyles(theme => ({
    subBody: {
        // position: 'fixed',
        height:HEIGHT,
        marginTop: theme.spacing(isMobile ? 8 :0),
        display: 'flex',
        flexDirection: 'column',
        // justifyContent: "center"
    },
    grid:{
        display: 'flex',
        flexDirection: 'column',
    },
    paper:{
        // height:'100%',
        // maxHeight:HEIGHT,
        // minWidth:300,
        // width:MAXWIDTH,
        maxWidth:MAXWIDTH,
        display: 'flex',
        flexDirection: 'column',
    }
}));

export default function Main() {
    const classes = useStyles();

    return (
        <div className={classes.subBody}>
            <Router basename={getPathBase()}>
                <Switch>
                    <Route path='/transfer' component={SignTransaction} />
                    <Route path='/'>
                        <WalletBar />
                        <Routes />
                    </Route>
                </Switch>
            </Router>
        </div>
    )
}
