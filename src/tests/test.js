const ethers = require('ethers');
const web3 = require('web3');
// const utils = require('../utils');
// import {ethers} from 'ethers'
// import {web3} from 'web3'
// import {convertHexToString} from '../utils/index.js'
const ERC20_ABI = require('../constants/abis/ERC20ABI.json')

const BLOCK_PAGING_NUMBER = 20
let provider = new ethers.providers.JsonRpcProvider('http://localhost:7545','homestead');
// let web3 = new web3(Web3.givenProvider || "http://localhost:7545");
const wallet = new ethers.Wallet('43e0886e6701a2b4eb9b4195cf56977b961d5d2b7bd07c631a08e85649bb50bc', provider);
const address = '0xa332c20E01a8acBefCd694958eA5364EE284f839'
const contract_addr = '0x996B180E235F1d8640009E9E15C7A4A7e830D3cb'
const erc20 = new ethers.Contract(contract_addr,ERC20_ABI,provider)

const NET_WORKS_NAME = [
    '网络',
    '以太坊主网络',
    'Ropsten 测试网络',
    'Rinkeby 测试网络',
    'Kovan 测试网络',
    'Local 测试网络'
];

// NET_WORKS_NAME.map((net,key)=> console.log('key===',key))
const NETWORKS = [
    {
        id:1,
        network:'homestead',
        name:'以太坊主网络'
    },
    {
        id:3,
        network:'ropsten',
        name:'Ropsten 测试网络'
    },
    {
        id:4,
        network:'rinkeby',
        name:'Rinkeby 测试网络'
    },
    {
        id:42,
        network:'kovan',
        name:'Kovan 测试网络'
    },
    {
        id:1337,
        network:'local',
        name:'Local 测试网络'
    }
]
// NETWORKS.map((item)=> console.log('net==',item.name))


async function main() {
    console.log(`Loaded wallet ${wallet.address}`);
    provider.getBlockNumber().then(function(res){
        console.log('get block number', res);
    });
    // provider.getTransaction('0x44112c6a6e4d143b8383cbd4074c60a1ed7fac369450dd9ad149473bee10ed56').then((transaction) => {
    //     console.log('gasPrice default:',transaction.gasPrice);
    //     console.log(web3.utils.toDecimal(transaction.gasPrice));
    //     console.log((typeof transaction.value._hex === 'object') ? 'obj':'string');
    //     console.log(ethers.utils.isHexString(transaction.value._hex));
    //     console.log('gesPrice in eth:',ethers.utils.formatEther(transaction.gasPrice._hex));
    //     console.log('gesPrice in gwei:',ethers.utils.formatUnits(transaction.gasPrice._hex, 'gwei'));

    //     console.log('utils gesPrice in gwei:',convertHexToString(transaction.gasPrice, 'gwei'));
    //     console.log('utils gesPrice in eth:',convertHexToString(transaction.gasPrice, 'eth'));
    //     console.log('utils value in eth:',convertHexToString(transaction.value, 'eth',4));
    //     console.log('utils nonce:',convertHexToString(0));
    //     console.log('utils gasLimit:',convertHexToString(transaction.gasPrice));
    // });

    // console.log(erc20)

    erc20.symbol().then((res)=>{
        console.log('erc20 symbol: ',res);
    })

    erc20.balanceOf(address).then((res)=>{
        console.log('erc20 balance: ',ethers.utils.formatEther(res));
    })
    const input = '0xa9059cbb000000000000000000000000da8bff780b2c40416cecbf4b9990bebae3e08cec000000000000000000000000000000000000000000000006aaf7c8516d0c0000'

    console.log('convertHexToString: ',convertHexToString('0x'+input.substring(75,input.length).replace(/\b(0+)/gi,""),'eth',4))

    let arrFound = NETWORKS.map(item => item.network);
    console.log('arrFound===',arrFound)
    // let arrFound = NETWORKS.filter(item => item.id == 3 );
    // console.log('arrFound===============',NETWORKS.map(item => item.id))

    NETWORKS.map(item => {
        console.log(item.network)
    })
    
}

function getLocalTransaction(tokenAddress,provider) {
    let transList = []
    try {
        let blockNumber 
        provider.getBlockNumber().then(function(res){
            blockNumber = res
        })
        let blockAmount = (blockNumber > BLOCK_PAGING_NUMBER) ? blockNumber - BLOCK_PAGING_NUMBER: blockNumber
        for(let i=blockNumber;i>=blockAmount;i--) {
            provider.send('eth_getBlockByNumber', [i, true]).then(blockTransactions => {
                // console.log('utils eth_getBlockByNumber', blockTransactions)
                blockTransactions.transactions.forEach((trans) => {
                    // console.log('utils trans', trans.hash)
                    if(trans?.input != '0x') {
                        // return true 
                    }
                    if(tokenAddress?.toUpperCase() == trans.to?.toUpperCase() || tokenAddress?.toUpperCase() == trans.from?.toUpperCase()) {
                        trans['timestamp'] = blockTransactions.timestamp.toString()//utils.bigNumberify(blockTransactions.timestamp)
                        trans.value = convertHexToString(trans.value, 'eth', 4)
                        trans.nonce = convertHexToString(trans.nonce)
                        trans.gasPriceInGwei = convertHexToString(trans.gasPrice, 'gwei')
                        trans.gasPriceInEth = convertHexToString(trans.gasPrice, 'eth')
                        trans['gasLimit'] =  convertHexToString(trans.gas)
                        trans['data'] =  trans.input
                        transList.push(trans)
                    }
                })
            })
        }

        return transList.reverse()
    } catch (e){
        console.log('getLocalTransaction',e)
        return null
    }
}

function convertHexToString(object, type='', digits=0) {
    let res
    try {
        res = (typeof object === 'object') ? res = object?._hex : object
        
        if(ethers.utils.isHexString(res)) {
            switch (type) {
                case 'eth':
                    res = (digits > 0) ? (+ ethers.utils.formatEther(res)).toFixed(digits) : ethers.utils.formatEther(res)
                break;
                case 'gwei':
                    res = ethers.utils.formatUnits(res, 'gwei')
                break;
                default:
                    console.log('ethers bignumber:',ethers.utils.formatUnits(res, "wei"))
                    res = web3.utils.toDecimal(res)       
                    console.log('web3333333333333333333333333333')          
            }            
        } else {
            console.log(typeof res)
        }
    } catch(e) {
        console.log('convertHexToString',e)
        return ""
    }

    return res?.toString()
}

main().catch((error) => {
    console.error(error);
});
