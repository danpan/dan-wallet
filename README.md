

## 一个仿MetaMask的简单的网页版以太坊钱包

####
- 实现了账号的新建、导入、导出功能 
- ERC20的代币添加、隐藏、交易功能 
- ETH发送功能 
- 签名交易功能 
####

### ❤️ Forked from https://gitee.com/TianCaoJiangLin/khwallet

### ✨ 新加功能

**1. 添加Local Ganache**<br/>
**2. 添加Transaction List**<br/>
**3. 添加多账户及账户名标识**<br/>
**4. 替换jazzicon 为 react-jazzicon，原jazzicon有BUG**<br/>
**5. 添加设置网络**<br/>


### ⛱️ 截图
<img src="src/components/assets/dan-wallet1.png" width="300" style="margin:10px"/>
<img src="src/components/assets/dan-wallet3.png" width="300" style="margin:10px" /><br/>
<img src="src/components/assets/dan-wallet2.png" width="300" style="margin:10px"/>
<img src="src/components/assets/dan-wallet4.png" width="300" style="margin:10px" /><br/>
<br/><br/>

### 🚧 前置
####
- 注册申请infura project id / project secret用于交易 : https://infura.io/
- 注册申请etherscan key用于跳转查询交易详情 : https://etherscan.io/apis
- 把 .env_default 文件改为 .env，并修改你的 etherscan key 和 infura project id
- 按需至 https://faucet.metamask.io/ 获取Ropsten ETH的测试TOKEN (打开这个网站前要在浏览器安装metamask，否则提示No ethereum provider detected)
- 按需安装Ganache用于测试ERC20 TOKEN，/src/tests/里有两种用于测试的合约，可用truffle编译，具体命令参考：https://ethereum.org/en/developers/tutorials/create-and-deploy-a-defi-app/
####

```bash
# 安装依赖
npm install

# 运行项目
npm start

```
