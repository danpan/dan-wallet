// //网络名称
// export const NET_WORKS_NAME = [
//     '网络',
//     '以太坊主网络',
//     'Ropsten 测试网络',
//     'Rinkeby 测试网络',
//     'Kovan 测试网络',
//     'Local 测试网络'
// ];
// //网络
// export const NET_WORKS = [
//     'network',
//     'homestead',
//     'ropsten',
//     'rinkeby',
//     'kovan',
//     'local'
// ];
// //网络的chainId
// export const CHAINID_TO_NETWORK = {
//     1:'homestead',
//     3:"ropsten",
//     4:"rinkeby",
//     42:'kovan',
//     1337:'local'
// }

// export const ETHERSCAN_PREFIXES = {
//     1: '',
//     3: 'ropsten.',
//     4: 'rinkeby.',
//     42: 'kovan.',
// }

export const NETWORKS = [
    {
        id:1,
        network:'homestead',
        name:'以太坊主网络',
        browser:'https://etherscan.io',
        symbol:'ETH'
    },
    {
        id:3,
        network:'ropsten',
        name:'Ropsten 测试网络',
        browser:'https://ropsten.etherscan.io',
        symbol:'ETH'
    },
    {
        id:4,
        network:'rinkeby',
        name:'Rinkeby 测试网络',
        browser:'https://rinkeby.etherscan.io',
        symbol:'ETH'
    },
    {
        id:42,
        network:'kovan',
        name:'Kovan 测试网络',
        browser:'https://kovan.etherscan.io',
        symbol:'ETH'
    },
    {
        id:1337,
        network:'local',
        name:'Local Ganache 测试',
        browser:'',
        symbol:'ETH',
        rpc:'http://localhost:7545'
    },
]

//实现721接口
export const _INTERFACE_ID_ERC721 = "0x80ac58cd";
//实现可列举接口
export const _INTERFACE_ID_ERC721_ENUMERABLE = "0x780e9d63";
//实现元数据接口
export const _INTERFACE_ID_ERC721_METADATA = "0x5b5e139f";
//实现获取所有ID接口
export const _INTERFACE_ID_TOKEN_BALANCES = "0xc84aae17";

export const ETHERSCAN_API_KEY = process.env.REACT_APP_ETHERSCAN_API_KEY;

export const INFURA_PROJECT_ID = process.env.REACT_APP_INFURA_PROJECT_ID;

export const URL_LOCAL_GANACHE = 'http://localhost:7545'

export const BLOCK_PAGING_NUMBER = 50

export const ETH_INPUT_DATA = '0x'

export const ERC20_INPUT_DATA = '0xa9059cbb'

export const ICON_SIZE = 50

export const INTERVAL = 60000;
//只有主网络才有查询并显示ETH价格的必要
export const MAINNET = 'homestead'
//eth_price_query_url
//这里YourApiKeyToken要替换成你自己的ApiKey
export const CN_ETHERSCAN_QUERY_URL = 'https://api-cn.etherscan.com/api?module=stats&action=ethprice&apikey=' + ETHERSCAN_API_KEY
export const ETHEREUM_PREv = 'ethereum:'

export const _NETWORKS = '_NETWORKS'