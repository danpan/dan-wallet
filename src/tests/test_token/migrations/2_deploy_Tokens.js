const TestToken = artifacts.require("TestToken")
const FarmToken = artifacts.require("FarmToken")

module.exports = async function (deployer, network, accounts) {
  // Deploy TestToken
  await deployer.deploy(TestToken)
  const testToken = await TestToken.deployed()

  // Deploy Farm Token
  await deployer.deploy(FarmToken, testToken.address)
  const farmToken = await FarmToken.deployed()
}
