import crypto from 'crypto'
import {utils,ethers} from 'ethers'
import ERC20_ABI from 'constants/abis/ERC20ABI.json'
import ERC721_ABI from 'constants/abis/ERC721ABI.json'
import {NETWORKS,_NETWORKS,BLOCK_PAGING_NUMBER,
    ETH_INPUT_DATA,ERC20_INPUT_DATA,ICON_SIZE,CN_ETHERSCAN_QUERY_URL,ETHERSCAN_PREFIXES} from '../constants'
import * as dayjs from 'dayjs';
import * as relativeTime from 'dayjs/plugin/relativeTime';
// import * as utc from 'dayjs/plugin/utc';
// import * as updateLocale from 'dayjs/plugin/updateLocale';
import React from 'react';
import Jazzicon, { jsNumberForAddress } from 'react-jazzicon'
import axios from 'axios'

// dayjs.extend(updateLocale);
// dayjs.extend(utc);
dayjs.extend(relativeTime);

//一般用不到，标记错误代码
export const ERROR_CODES = ['TOKEN_NAME', 'TOKEN_SYMBOL','TOKEN_NAME','TOKEN_DECIMALS','TOKEN_BALANCE','TOKEN_INTERFACE'].reduce(
  (accumulator, currentValue, currentIndex) => {
    accumulator[currentValue] = currentIndex
    return accumulator
  },
  {}
)

//aes加密
export function aesEncrypt(data, key) {
    let cipher = crypto.createCipher('aes192', key);
    let crypted = cipher.update(data, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

//aes解密
export function aesDecrypt(encrypted, key) {
    let decipher = crypto.createDecipher('aes192', key);
    let decrypted = decipher.update(encrypted, 'hex', 'utf8');
    decrypted += decipher.final('utf8');
    return decrypted;
}

//获取设定的密码长度
export function getPasswordLength() {
    let length = process.env.REACT_APP_PASSWORD_LENGTH;
    return + length
}

//将以太坊地址取首尾几个字符，用较短形式显示
export function shortenAddress(address, digits = 4) {
    return `${address?.substring(0, digits + 2)}...${address?.substring(address.length - digits)}`
}

//将ether数量（单位为wei)转化为相应的十进制小数（单位为ETH)
export function convertToEth(_bigNumber) {
    if(!_bigNumber) {
        return 0
    }
    let eth_string = utils.formatEther(_bigNumber)
    return + eth_string
}

//将任意精度的token余额转换成为十进制小数 。例如 0x1234 => 12.34
//第一个参数类型为bigNumber，为用户代币余额，第二个参数类型为十进制整数，为代币的精度
export function convertBigNumberToFixed(_bigNumber,decimals) {
    if(!_bigNumber) {
        return 0
    }
    let _str = _bigNumber.toString()
    let _len = _str.length;
    if(_len < decimals - 3){
        return 0
    }
    if(_len > decimals) {
        let header = _str.substring(0,_len - decimals);
        let body = _str.substring(_len - decimals,
            _len - decimals + 4 > _len ? _len - decimals + 4 : _len)
        let result = header + "." + body
        return + result
    }
    let _step = decimals - _len
    let _result = "0."
    for(let i=0;i<_step;i++){
        _result.append("0")
    }
    let _tail = _str.substring(0,4-_step)
    _result = _result.concat(_tail)
    return + _result
}

//同上一个方法相反，将十进制小数转换成为做任意精度的token余额 例如  12.34 => 0x1234
//第一个参数类型为十进制小数，为用户设定的数量，第二个参数类型为十进制整数，为代币的精度
export function convertFixedToBigNumber(fixed,decimals){
    if(!fixed) {
        return ethers.constants.Zero
    }
    let strs = fixed.toString().split('.')
    let power = 0;
    let r_str = strs[0]
    if(strs.length > 1) {
        let _tail = strs[1]
        let _len = _tail.length
        if(_len > decimals){
            _tail = _tail.substring(0,decimals)
        }
         r_str = r_str + _tail
         power = _tail.length
    }
    let ten = utils.bigNumberify(10)
    let times = ten.pow(power)
    let times_str = utils.bigNumberify(r_str)
    return times_str.mul(ten.pow(decimals)).div(times)
}

//用来获取对象中深层属性，比如获取 a.b.c.d
//第一个参数是a，第二个参数为数组，为属性列表[b,c,d]
export function safeAccess(object, path) {
    // console.log('path====' + path)
    return object
        ? path.reduce((accumulator, currentValue) => (
            accumulator && accumulator[currentValue]
            ? accumulator[currentValue]
            : null), object)
        : null
}

//将一个支持的地址转成校验过的地址，同时也可以用来判定地址是否有效
export function isAddress(_address) {
    let address = null;
    try{
       address = utils.getAddress(_address)
    }catch{
    }
    return address
}

//获取etherscan查看链接
export function getEtherscanLink(networks,chainId, data, type) {
    const net = networks.filter(item => item.id === chainId )

    const prefix = (net) ? net[0].browser : ''
    // const prefix = `https://${ETHERSCAN_PREFIXES[network] || ETHERSCAN_PREFIXES[0]}etherscan.io`

    switch (type) {
        case 'transaction': {
            return `${prefix}/tx/${data}`
        }
        case 'token':{
            return `${prefix}/token/${data}`
        }
        case 'address':
        default: {
            return `${prefix}/address/${data}`
        }
    }
}

//下面的网络名称特指全称（比如Kovan测试网络、主网等），而网络特指它的字符串代表（如'homestead'、'kovan'）
//根据chainId获取网络
export function getNetworkByChainId(networks,chainId) {
    return networks.filter(item => item.id === chainId )[0].network
    // return CHAINID_TO_NETWORK[chainId]
}

//和上面的相反，根据网络获取chainId
export function getChainIdByNetwork(networks,network) {
    if(!network) {
        return 1
    } else {
        let net = networks.filter(item => item.network === network )[0]
        return (network === 'local') ? net?.id   : utils.getNetwork(network).chainId
    }

    // return (network === 'local') ? getKeyByValue(CHAINID_TO_NETWORK,network).value : utils.getNetwork(network).chainId
}

//获取网络名称
export function getNetworkName(networks,network) {
    let nw =  networks.filter(it => it.network === network);
    return nw.length > 0 ? nw[0].name : ''
}

//根据chainId获取网络名称
export function getNetworkNameById(networks,chainId) {
    return getNetworkName(networks,getNetworkByChainId(networks,chainId))
}

//获取ERC20代币合约对象并链接到相应钱包
//参数分别为代币地址，网络和钱包
export function getErc20Token(tokenAddress,network,wallet,provider) {
    if(!isAddress(tokenAddress) || !network) {
        return null;
    }
    // const {provider} = useGlobal()
    try{
        // let provider = ethers.getDefaultProvider(network);
        if(wallet) {
            provider = wallet.connect(provider)
        }
        return new ethers.Contract(tokenAddress,ERC20_ABI,provider)
    }catch{
        return null
    }
}
//下面三个代币相关的方法中的代币合约对象均已使用上面的方法链接到钱包。
//获取token符号
export async function getTokenSymbol(tokenContract) {
    return tokenContract.symbol().catch(error => {
        error.code = ERROR_CODES.TOKEN_SYMBOL
        throw error
    })
}

//获取token精度
export async function getTokenDecimals(tokenContract) {
    return tokenContract.decimals().catch(error => {
        error.code = ERROR_CODES.TOKEN_DECIMALS
        throw error
    })
}

//获取某个地址在某个token余额
export async function getTokenBalance(tokenContract,address) {
    return tokenContract.balanceOf(address).catch(error => {
        error.code = ERROR_CODES.TOKEN_BALANCE
        throw error
    })
}

//获取ERC721代币合约对象并链接到相应钱包
//参数分别为代币地址，网络和钱包
export function getErc721Token(tokenAddress,network,wallet,provider) {
    // const {provider} = useGlobal()
    if(!isAddress(tokenAddress) || !network) {
        return null;
    }
    try{
        // let provider = ethers.getDefaultProvider(network);
        if(wallet) {
            provider = wallet.connect(provider)
        }
        return new ethers.Contract(tokenAddress,ERC721_ABI,provider)
    }catch{
        return null
    }
}
//下面三个代币相关的方法中的代币合约对象均已使用上面的方法链接到钱包。
//获取token符号
export async function getERC721TokenSymbol(tokenContract) {
    return tokenContract.symbol().catch(error => {
        error.code = ERROR_CODES.TOKEN_SYMBOL
        throw error
    })
}

//获取token精度
export async function getERC721TokenName(tokenContract) {
    return tokenContract.name().catch(error => {
        error.code = ERROR_CODES.TOKEN_NAME
        throw error
    })
}

//获取某个地址在某个token余额(总数量)
export async function getERC721TokenBalance(tokenContract,address) {
    return tokenContract.balanceOf(address).catch(error => {
        error.code = ERROR_CODES.TOKEN_BALANCE
        throw error
    })
}
//获取某个地址在某个token的所有ID
export async function getErc721TokenIds(tokenContract,address){
    return tokenContract.getBalances(address).catch(error => {
        error.code = ERROR_CODES.TOKEN_IDS
        throw error
    })
}

export async function getSupportInterface(tokenContract,signature) {
    return tokenContract.supportsInterface(signature).catch(error => {
        error.code = ERROR_CODES.TOKEN_INTERFACE
        throw error
    })
}

//将一个html节点转换成字符串
export function nodeToString(node) {
    let tmpNode = document.createElement( "div" );
    tmpNode.appendChild( node.cloneNode( true ) );
    let str = tmpNode.innerHTML;
    tmpNode = node = null; // prevent memory leaks in IE
    return str;
}

//将一个query对象转换成交易对象
export function convertQuery(networks,query) {
    let gasLimit = query.get('gasLimit')
    let nonce = + query.get('nonce')
    let to = query.get('to')
    let gasPrice = + query.get('gasPrice')
    let value = + query.get('value')
    let data = query.get("data")
    let chainId = getChainIdByNetwork(networks,query.get("net") || "")
    let result = {}
    try{
        if(to) {
            result.to = to
        }
        if(nonce) {
            result.nonce = nonce
        }
        if(gasLimit) {
            result.gasLimit = utils.bigNumberify(gasLimit)
        }
        if(gasPrice) {
            result.gasPrice = gasPrice
        }
        if(value) {
            result.value = value
        }
        if(data) {
            result.data = data
        }
        if(chainId) {
            result.chainId = chainId
        }
    }catch(e) {
        return {}
    }
    return result
}

//获取路由的基准路径，注意在开发环境和生产环境的区别
export function getPathBase() {
    return process.env.NODE_ENV === 'production' ? process.env.REACT_APP_PATH_BASE : ''
}

export async function getErc20TransactionList(tokenAddress,token,network,provider) {
    if(!isAddress(tokenAddress) || !network) {
        return null
    }

    try{
        let promise
        if(network === 'local') {
            // console.log('utils provider', provider)
            promise = getLocalTransaction(tokenAddress,token.address,token.symbol,provider)
            // console.log('getLocalTransaction', promise)            
        } else {
            // console.log(getChainIdByNetwork(network))
            promise = new ethers.providers.EtherscanProvider(network, process.env.REACT_APP_ETHERSCAN_API_KEY).getHistory(tokenAddress)
        }
        
        // let transList = []
        // etherscanProvider.getHistory(tokenAddress).then((history) => {
        //     history.forEach((tx) => {
        //         transList.push(tx)
        //     })
        // });
        // console.log('utils transList length',transList.length)
        // return transList
        return promise
    } catch (e){
        console.log('getErc20TransactionList',e)
        return null
    }
}

async function getLocalTransaction(tokenAddress,contractAddress,symbol,provider) {
    let transList = []
    try {
        let blockNumber = await provider.getBlockNumber()

        let blockAmount = (blockNumber >= BLOCK_PAGING_NUMBER -1 ) ? blockNumber - BLOCK_PAGING_NUMBER +1: blockNumber
        for(let i=blockNumber;i>=blockAmount;i--) {
            await provider.send('eth_getBlockByNumber', [i, true]).then(blockTransactions => {
                let transAmount = null
                // console.log('utils eth_getBlockByNumber', blockTransactions)
                blockTransactions.transactions.forEach((trans) => {
                    // check if eth or erc20 token
                    if(symbol !== 'ETH' && (contractAddress.toUpperCase() === trans.to?.toUpperCase() || contractAddress.toUpperCase() === trans.from?.toUpperCase())) { 
                        if(trans?.input.substring(0,10) === ERC20_INPUT_DATA) {
                            const tmpAddr = ETH_INPUT_DATA + trans?.input.substring(34,74)
                            
                            if(tokenAddress?.toUpperCase() === tmpAddr.toUpperCase()) {
                                trans.to = tokenAddress
                            } else {
                                trans.from = tokenAddress
                            }
                            transAmount = getErc20AmountByInputData(trans.input)
                        } else {
                            return true
                        }
                        
                        transList.push(formatTransactionData(trans, blockTransactions, transAmount, symbol))
                    } 
                    
                    if(symbol === 'ETH' && trans.input === ETH_INPUT_DATA && (tokenAddress.toUpperCase() === trans.to?.toUpperCase() || tokenAddress.toUpperCase() === trans.from?.toUpperCase())) {
                        transList.push(formatTransactionData(trans, blockTransactions, transAmount, symbol))
                    }
                })
            })
        }
        return transList.reverse()
    } catch (e){
        console.log('getLocalTransaction',e)
        return null
    }
}

export function isSender(from, address) {
    if(from.toString().toUpperCase().trim() === address.toString().toUpperCase().trim()) {
        return true
    }
    return false
}

function formatTransactionData(trans, blockTransactions, transAmount, symbol) {
    trans['timestamp'] = blockTransactions.timestamp.toString()//utils.bigNumberify(blockTransactions.timestamp)
    trans.value = convertHexToString((transAmount) ? transAmount : trans.value, 'eth', 4)
    trans.nonce = convertHexToString(trans.nonce)
    trans.gasPriceInGwei = convertHexToString(trans.gasPrice, 'gwei')
    trans.gasPriceInEth = convertHexToString(trans.gasPrice, 'eth')
    trans['gasLimit'] =  convertHexToString(trans.gas)
    trans['data'] =  trans.input
    trans['symbol'] =  symbol
    return trans
}

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}

function getErc20AmountByInputData(data) {
    return ETH_INPUT_DATA + data.substring(75, data.length).replace(/\b(0+)/gi,"")
}

export function convertHexToString(object, type='wei', digits=0) {
    let res
    try {
        res = (typeof object === 'object') ? res = object?._hex : object
        
        if(ethers.utils.isHexString(res)) {
            switch (type) {
                case 'eth':
                    res = (digits > 0) ? (+ ethers.utils.formatEther(res)).toFixed(digits) : ethers.utils.formatEther(res)
                break;
                case 'int':
                    res = parseInt(ethers.utils.formatUnits(res, type))
                break;
                default: // gwei, wei
                    // res = web3.utils.toDecimal(res) 
                    res = ethers.utils.formatUnits(res, type)
                    
            }            
        } else {
            if(type === 'int') {
                res = parseInt(res)
            }
        }
    } catch(e) {
        console.log('convertHexToString',e)
        console.log(object)
        return ""
    }

    return res?.toString()
}

export function getEthPrice() {
    let price = 0
    axios.get(CN_ETHERSCAN_QUERY_URL).then(rep => {
        if(rep.data.status === '1'){
            price = rep.data.result.ethusd
        }
    }).catch(() =>{})
    return price
}

export function TokenIcon({tokenAddress,size=ICON_SIZE}) {
    // const iconToken = useAddressIcon(tokenAddress,size)
    // const refToken = useRef()

    // useEffect(()=>{
    //     if(refToken.current) {
    //         refToken.current.appendChild(iconToken)
    //     }
    // },[iconToken])
    if(!tokenAddress) {
        return ''
    }

    return (
        <Jazzicon diameter={size} seed={jsNumberForAddress(tokenAddress)} />
    )
}

export function validURL(str) {
    //eslint-disable-next-line
    const pattern = new RegExp( '^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$');
    return !!pattern.test(str);
}

export function isDefaultNetwork(network) {
    return NETWORKS.map(item => item.network).includes(network)
}
