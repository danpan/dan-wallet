/**
*  本文件用来判断是显示代币列表还是显示增加代币页面
*/
import React from 'react'
import Network from './Network'
import { withRouter } from "react-router";
import {useGlobal,useUpdateGlobal} from 'contexts/GlobalProvider.js'

function Setting({history}) {
    const {isShowSetting} = useGlobal()
    const updateGlobal = useUpdateGlobal()
    const onClose = () => {
        updateGlobal({
            isShowSetting:false,
        })
        history.goBack()
    }

    return (
        <Network onClose={onClose}  />
    )

}

export default withRouter(Setting)
