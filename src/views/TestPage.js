/* eslint-disable */
import React, {useState,useReducer,useCallback} from 'react';
import {makeStyles,withStyles} from '@material-ui/core/styles';
import { withRouter } from "react-router";
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {NETWORKS,_NETWORKS} from '../constants'
import {useStorageByTag} from 'contexts/StorageProvider'

const useStyles = makeStyles(theme => ({

    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: theme.spacing(3)
    },
    root:{
        '&:hover': {
            backgroundColor: '#fff',
          },
          '&$focused': {
            backgroundColor: '#fff',
            borderColor: theme.palette.primary.main,
          },
    },

}));

const ValidationTextField = withStyles({
    root: {
      '& input:valid + fieldset': {
        borderColor: 'green',
        borderWidth: 2,
      },
      '& input:invalid + fieldset': {
        borderColor: 'red',
        borderWidth: 2,
      },
      '& input:valid:focus + fieldset': {
        borderLeftWidth: 6,
        padding: '4px !important', // override inline-style
      },
    },
  })(TextField);



function TestPage({history}) {
    const classes = useStyles();
    const newNetworks = useStorageByTag(_NETWORKS);

    let result = {...NETWORKS,...newNetworks} //Object.keys(newNetworks).map(key => {NETWORKS.push(newNetworks[key])});

    console.log(NETWORKS);
    console.log(result);




    return(
        <div className={classes.container}>
            <form className={classes.form} >
            <FormControl margin="normal"  fullWidth>
                <TextField id="block-browser-url-input"
                    label='区块浏览器 URL'
                    variant="outlined"
                    type="text"
                    value=''
                />
            </FormControl>
            </form>
        </div>
    )
}

export default withRouter(TestPage)

