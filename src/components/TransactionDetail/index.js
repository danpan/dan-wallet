import React from 'react';
import {ThemeProvider,makeStyles,createMuiTheme} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DialogContent from '@material-ui/core/DialogContent';
import copy from 'copy-to-clipboard';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import CaretRight from '@material-ui/icons/ArrowForwardIos';
import Divider from '@material-ui/core/Divider';
import {useGlobal} from 'contexts/GlobalProvider';
import DialogTitle from '@material-ui/core/DialogTitle';
import Tooltip from '@material-ui/core/Tooltip';
import {useSimpleSnackbar} from 'contexts/SimpleSnackbar.jsx';
import CopyIcon from '@material-ui/icons/FileCopyOutlined';
import PopoutIcon from '@material-ui/icons/CallMadeOutlined';
import {getEtherscanLink,getChainIdByNetwork,isSender,shortenAddress,convertHexToString} from 'utils'

const muiTheme = createMuiTheme({
    overrides: {
      MuiListItem: {
        root: {
            display:'block',
        },
      },
    },
});

const useStyles = makeStyles(theme => ({
    contentRoot:{
        padding: '0px'
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(0.5),
        color: theme.palette.grey[500],
    },
    detailHeader: {
        margin: '0px 8px 16px 16px',
        // marginLeft: '8px',
        // marginBottom: '16px',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    detailHeaderButtons: {
        display: 'flex',
        flexDirection: 'row',
    },
    detailHeaderButton: {
        marginRight: '8px'
    },
    ButtonRaised: {
        color: '#3ED4AE',
        backgroundColor: '#fff',
        boxShadow: '0 2px 4px rgb(0 0 0 / 15%)',
        padding: '6px 8px 5px 8px',
        height: 'initial',
        minHeight: 'initial',
        width: 'initial',
        minWidth: 'initial',
        "& svg":{
            wdith: '0.6em',
            height: '0.6em',
        },
    },
    detailBody: {
        background: '#fafbfc',
        // padding: '8px 16px',
    },
    senderToRecipient: {
        fontSize: '0.825rem',
        fontFamily: 'Euclid, Roboto, Helvetica, Arial, sans-serif',
        fontStyle: 'normal',
        fontWeight: 'normal',
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'relative',
        flex: '0 0 auto',
        padding: '6px 0px',
        minWidth: '0',
        color: '#9b9b9b',
    },
    senderToRecipientAccount:{
        display: 'flex',
        // flexDirection: 'row',
        alignItems: 'center',
        // justifyContent: 'center',
        // flex: '1',
        // padding: '6px',
        cursor: 'pointer',
        minWidth: '0',
        // color: '#9b9b9b',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
    },
    senderToRecipientIcon:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        "& svg":{
            height: '0.6em',
        },
    },
    breakdownRow : {
        fontSize: '0.75rem',
        fontFamily: 'Euclid, Roboto, Helvetica, Arial, sans-serif',
        lineHeight: '140%',
        fontStyle: 'normal',
        fontWeight: 'normal',
        color: '#5d5d5d',
        display: 'flex',
        justifyContent: 'space-between',
        padding: '8px 0',
    },
    breakdownRowTitle : {
        paddingRight: '8px',
    },
    breakdownRowValue : {

    },
    breakdownValue : {
        display: 'flex',
        justifyContent: 'flex-end',
        textAlign: 'end',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
    MuiListItem: {
        root: {
            display:'block',
        },
    },
}));

function TransactionDetail(props) {
    const classes = useStyles();
    const {wallet,networks,network} = useGlobal();
    const {address} = wallet;
    const showSnackbar = useSimpleSnackbar()

    const {openDetail,onClose,transItem} = props
    // console.log('transItem======',transItem)

    //返回主界面
    const handleCloseDetail = () => {
        onClose(transItem);
    }

    //复制交易ID到粘贴板
    const doCopy = (item) => {
        if(copy(item))
            showSnackbar("已经复制到粘贴板",'success')
    }
    //在etherscan上显示
    const showDetail = () => {
        let chainId = getChainIdByNetwork(networks,network)
        let url = getEtherscanLink(networks,chainId,transItem.hash,'transaction')
        window.open(url)
    }

    function showPanel(transItem,classes) {
        return (
            <>
                {/* <!-- Dialog中无法使用useRef... --> */}
                <DialogTitle id="customized-dialog-title" >
                    <div style={{textAlign:"center"}}>
                        {isSender(transItem.from, address) ? "发送":"接收"}
                    </div>
                    <IconButton aria-label="close" className={classes.closeButton} onClick={() => handleCloseDetail()}>
                        <CloseIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent className={classes.contentRoot}>
                    <div className={classes.detailHeader}>
                        <div>详情</div>
                        <div className={classes.detailHeaderButtons}>
                            <div className={classes.detailHeaderButtons}>
                                <Tooltip title="复制交易 ID" arrow className={classes.detailHeaderButton}>
                                    <button className={classes.ButtonRaised}  onClick={(event) => {event.preventDefault();doCopy(transItem.hash)} }>
                                        <CopyIcon />
                                    </button>
                                </Tooltip>
                                <Tooltip title="在 Etherscan（以太坊浏览器）上查看" arrow className={classes.detailHeaderButton}>
                                    <button className={classes.ButtonRaised}  onClick={showDetail}>
                                        <PopoutIcon />
                                    </button>
                                </Tooltip>
                            </div>
                        </div>
                    </div>
                    <div className={classes.detailBody}>
                        <ThemeProvider theme={muiTheme}>
                            <List>
                                <ListItem>
                                    <div className={classes.senderToRecipient}>
                                        <Tooltip title="复制到粘贴板" arrow >
                                            <div className={classes.senderToRecipientAccount} onClick={(event) => {event.preventDefault();doCopy(transItem.from)}} >{ '从: ' + shortenAddress(transItem.from, 10) }</div>
                                        </Tooltip>
                                        <div className={classes.senderToRecipientIcon}><CaretRight /></div>
                                        <Tooltip title="复制到粘贴板" arrow >
                                            <div className={classes.senderToRecipientAccount} onClick={(event) => {event.preventDefault();doCopy(transItem.to)}} >{' 至: ' + shortenAddress(transItem.to, 10) } </div>
                                        </Tooltip>
                                    </div>
                                </ListItem>
                                <ListItem divider>
                                    <div>交易</div>
                                </ListItem>                                
                                <ListItem>
                                    <div className={classes.breakdownRow}>
                                        <div className={classes.breakdownRowTitle}>数额</div>
                                        <div className={classes.breakdownRowValue}>
                                            <span className={classes.breakdownValue}>
                                            {isSender(transItem.from, address) ? "-":""} { convertHexToString(transItem.value,'eth',4) } {transItem.symbol}</span>
                                        </div>
                                    </div>
                                </ListItem>
                                <Divider light />
                                <ListItem>
                                    <div className={classes.breakdownRow}>
                                        <div className={classes.breakdownRowTitle}>Nonce</div>
                                        <div className={classes.breakdownRowValue}>
                                            <span className={classes.breakdownValue}> { convertHexToString(transItem.nonce, 'int') }</span>
                                        </div>
                                    </div>
                                </ListItem>
                                <Divider light />
                                <ListItem>
                                    <div className={classes.breakdownRow}>
                                        <div className={classes.breakdownRowTitle}>燃料限制 (数量)</div>
                                        <div className={classes.breakdownRowValue}>
                                            <span className={classes.breakdownValue}>
                                            { convertHexToString(transItem.gasLimit) }</span>
                                        </div>
                                    </div>
                                </ListItem>
                                <Divider light />
                                <ListItem>
                                    <div className={classes.breakdownRow}>
                                        <div className={classes.breakdownRowTitle}>燃料价格</div>
                                        <div className={classes.breakdownRowValue}>
                                            <span className={classes.breakdownValue}>
                                            { convertHexToString(transItem.gasPrice,'eth') } {transItem.symbol} ({ convertHexToString(transItem.gasPrice, 'gwei') } Gwei)</span>
                                        </div>
                                    </div>
                                </ListItem>
                                <Divider light />
                                <ListItem>
                                    <div className={classes.breakdownRow}>
                                        <div className={classes.breakdownRowTitle}>总额</div>
                                        <div className={classes.breakdownRowValue}>
                                            <span className={classes.breakdownValue}>
                                            { convertHexToString(transItem.value,'eth',4) } {transItem.symbol}</span>
                                        </div>
                                    </div>
                                </ListItem>
                            </List>
                        </ThemeProvider>
                    </div>
                </DialogContent>
            </>
        )
    }

    //最后渲染
    return (
        <Dialog
            fullWidth
            maxWidth='xs'
            open={openDetail}
            onClose={handleCloseDetail}
            aria-labelledby="transaction-dialog-title"
            aria-describedby="transaction-dialog-description"
        >
            {showPanel(transItem,classes)}
        </Dialog>
    )
}

export default TransactionDetail
