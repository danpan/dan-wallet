//本JS进行一些notistack的常用设置
//notistack是第三方包装的Material UI的消息条，
//目前版本在关闭回调的使用上还有一些问题，请注意
import React from 'react';
import { SnackbarProvider } from 'notistack';
import { isMobile } from 'react-device-detect';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import SimpleSnackbarProvider from './SimpleSnackbar.jsx';
import { makeStyles } from "@material-ui/core/styles";
/**
* 显示的消息条的最大数量，如果超过，会关掉最先打开的然后再显示新的，是一个队列
* 如果只想显示1个，设置为1，3是默认值
*/
const MAX_SNACKBAR = 3
//设置自动隐藏时间，默认值是5秒
const AUTO_HIDE_DURATION = 3000
//设置消息条位置，默认值为底部左边
//由于本工程在桌面端只显示为中间一小部分，所以调整了消息条的位置为中间，如果是全屏应用则不需要
const POSITION = {
    vertical: 'bottom',
    horizontal: isMobile ? 'left' : 'center'
}

const useStyles = makeStyles(() => ({
    success: { backgroundColor: '#3FD362' },
    error: { backgroundColor: '#D3553F' },
    warning: { backgroundColor: '#D3833F' },
    info: { backgroundColor: '#3FA5D3' },
}))

export default function NotistackWrapper({children}) {
    const classes = useStyles();
    const notistackRef = React.createRef();
    const onClickDismiss = key => () => {
        notistackRef.current.closeSnackbar(key);
    }

    return (
        <SnackbarProvider
            classes={{
                variantSuccess: classes.success,
                variantError: classes.error,
                variantWarning: classes.warning,
                variantInfo: classes.info,
            }}
            maxSnack={MAX_SNACKBAR}
            autoHideDuration={AUTO_HIDE_DURATION}
            anchorOrigin={POSITION}
            dense={isMobile}
            ref={notistackRef}
            action={(key) => (
                <IconButton key="close" aria-label="Close" color="inherit" onClick={()=>{onClickDismiss(key)}}>
                    <CloseIcon style={{fontSize:"20px"}}/>
                </IconButton>
            )}
        >
            <SimpleSnackbarProvider>
                {children}
            </SimpleSnackbarProvider>
        </SnackbarProvider>
    )
}
