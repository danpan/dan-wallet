import React from 'react';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import WalletIcon from 'components/assets/wallet.png';
// import amber from '@material-ui/core/colors/amber';

const useStyles = makeStyles(theme => ({
    icon: {
        width: 40,
        height: 40
    },
    container:{
        display:"flex",
        justifyContent:"center",
    },
    grow: {
        marginTop: theme.spacing(0.8),
        color:"#3FD3AC",
        fontWeight:"bold",
        fontSize: 23,
        fontFamily: 'Euclid, Roboto, Helvetica, Arial, sans-serif',
        marginLeft: 5
    }
}));

export default function Logo() {
    const classes = useStyles();

    return (<div className={classes.container}>
        <img src={WalletIcon} alt="MyWallet" className={classes.icon}/>
        <Typography className={classes.grow}>
            MyWallet
        </Typography>
    </div>)
}
