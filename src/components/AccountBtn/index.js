import React, {useState, useRef} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import {grey} from '@material-ui/core/colors';
import Menu from '@material-ui/core/Menu';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import {useGlobal,useUpdateGlobal} from 'contexts/GlobalProvider.js'
import Typography from '@material-ui/core/Typography';
import ListItemText from '@material-ui/core/ListItemText';
import {shortenAddress,TokenIcon,aesDecrypt} from 'utils';
import AddIcon from '@material-ui/icons/Add';
import VerticalAlignBottomIcon from '@material-ui/icons/VerticalAlignBottom';
import SettingsIcon from '@material-ui/icons/Settings';
import AccountDetail from '../AccountDetail';
import ImportAccount from '../ImportAccount';
import CreateAccount from '../CreateAccount';
import {ethers} from 'ethers';
import {INFURA_PROJECT_ID,URL_LOCAL_GANACHE} from '../../constants'
import { withRouter } from "react-router";

const useStyles = makeStyles(theme => ({
    accountBtn:{
        paddingLeft:theme.spacing(3),
        paddingRight:theme.spacing(3),
        borderRadius:20,
    },
    icon:{
        marginRight:theme.spacing(2),
    },
    menuPaper:{
        border: '1px solid #d3d4d5',
        backgroundColor:"#000000BB",
        color:'white',
        width: '220px',
    },
    dividerRoot:{
        backgroundColor:grey[700]
    },
    iconButton:{
        padding: '0px',
        fontSize: '0px',
    },
    menuItem:{
        padding: '12px',
        "&:hover": {
            backgroundColor: '#555 !important',
        }
    },
    currentAccountIcon:{
        height: '34px',
        width: '34px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderStyle: 'solid',
        borderRadius: '50%',
        borderWidth: '2px',
        borderColor: '#00bfa5',
    },
    accBtnTitle:{
        marginLeft:'15px'
    },
    addressItem:{
        margin:'0px 0px 0px 10px',
    },
    addressItemSecondary: {
        color:'#aaa'
    },
    isImportedTag:{
        fontSize: '0.5rem',
        fontFamily: 'Euclid, Roboto, Helvetica, Arial, sans-serif',
        lineHeight: '140%',
        borderRadius: '10px',
        padding: '2px 4px',
        textAlign: 'center',
        height: '15px',
        marginRight: '10px',
        backgroundColor: '#9b9b9b',
        color: '#000',
        letterSpacing: '0.5px',
        alignItems: 'center',
    }
}));



function AccountBtn({history}) {
    const classes = useStyles();
    const {wallet,allWallet,password,network,isShowSetting} = useGlobal()
    const anchorRefAccount = useRef(null)
    const updateGlobal = useUpdateGlobal()
    // const allAccounts = allWallet ? Object.keys(allWallet) : []
    console.log('allWallet=====',allWallet)

    const [state,setState] = useState({
        accountOpen:false,
        accountDetail:false,
        importAccount:false,
        createAccount:false,
        isShowSetting:false,
    })


    //弹出账号菜单
    const showAccountMenu = event => {
        event.preventDefault()
        setState({
            ...state,
            "accountOpen":true
        })
    }
    //关闭账号菜单
    const handleCloseAccount = event => {
        if (anchorRefAccount.current && anchorRefAccount.current.contains(event.target)) {
            return;
        }
        setState({
            ...state,
            "accountOpen":false
        })
    }
    //按键关闭账号菜单
    const handleListKeyDownAccount = event => {
        if (event.key === 'Tab') {
            event.preventDefault();
            setState({
                ...state,
                "accountOpen":false
            })
        }
    }
    //显示账号详情
    const showAccountDetail = (acc) => {
        setState({
            ...state,
            "accountOpen":false,
            // "accountDetail":true,
        })

        if(acc) {
            console.log('acc======',acc)
            let crypt = allWallet[acc].crypt
            let privateKey = allWallet[acc].privateKey
            let accountName = allWallet[acc].accountName
            console.log('crypt======',crypt)
            if (privateKey === undefined) {
                privateKey = aesDecrypt(crypt,password)
            }
            console.log('privateKey======',privateKey)
            let wallet = new ethers.Wallet(privateKey)

            if (network === 'local') {
                updateGlobal({
                    provider:new ethers.providers.JsonRpcProvider(URL_LOCAL_GANACHE),
                    tokenSelectedIndex:0,
                })
            } else {
                updateGlobal({
                    tokenSelectedIndex:0,
                    provider:new ethers.providers.InfuraProvider(network,INFURA_PROJECT_ID),
                })
            }

            let options = {
                wallet,
                accountName,
            }
            updateGlobal(options)
        }
    }
    //关闭账号详情界面
    const handleCloseDetail = () => {
        setState({
            ...state,
            "accountDetail":false,
        })
    }

    //显示导入账号
    const showImportAccount = event => {
        setState({
            ...state,
            "accountOpen":false,
            "importAccount":true,
        })
    }
    //关闭导入账号界面
    const handleCloseImport = () => {
        setState({
            ...state,
            "importAccount":false,
        })
    }

    //显示导入账号
    const showCreateAccount = event => {
        setState({
            ...state,
            "accountOpen":false,
            "createAccount":true,
        })
    }
    //关闭导入账号界面
    const handleCloseCreate = () => {
        setState({
            ...state,
            "createAccount":false,
        })
    }

    const showSetting = e => {
        e.preventDefault()
        setState({
            ...state,
            "accountOpen":false,
            "isShowSetting":true,
        })
        if(!isShowSetting){
            history.push('/setting')
            updateGlobal({
                isShowSetting:true,
            })
        }
    }

    const {accountOpen,accountDetail,importAccount,createAccount} = state

    function showAccountBtn(){
        return (
            <>
                <div className={classes.currentAccountIcon}>
                    <IconButton
                        ref={anchorRefAccount}
                        aria-controls={accountOpen ? 'menu-list-grow' : undefined}
                        color="inherit" aria-label="Menu"
                        aria-haspopup="true"
                        onClick={showAccountMenu.bind()}
                        className={classes.iconButton}
                    >
                        <TokenIcon tokenAddress={wallet.address} size={30} />
                    </IconButton>
                </div>
                <Menu
                    id="customized-menu-account"
                    anchorEl={anchorRefAccount.current}
                    keepMounted
                    open={accountOpen}
                    onClose={handleCloseAccount.bind()}
                    onKeyDown={handleListKeyDownAccount.bind()}
                    classes={{
                        paper:classes.menuPaper
                    }}
                    elevation={0}
                    getContentAnchorEl={null}
                    anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                    }}
                    transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                    }}
                >

                    <Typography variant='h6' classes={{root:classes.accBtnTitle}} gutterBottom>
                        我的账号
                    </Typography>

                    {
                        Object.keys(allWallet)?.map((acc) => {
                            return (
                                <MenuItem key={acc} onClick={showAccountDetail.bind(this,acc)} classes={{root:classes.menuItem}}>
                                    <TokenIcon tokenAddress={acc}  size={23}/>
                                    <ListItemText classes={{root:classes.addressItem,secondary:classes.addressItemSecondary}} primary={allWallet[acc].accountName} secondary={shortenAddress(acc).toUpperCase()} />
                                    {allWallet[acc].isImported ? <Typography classes={{root:classes.isImportedTag}} >已导入</Typography> : '' }
                                </MenuItem>
                            )
                        })
                    }
                    
                    <Divider classes={{root:classes.dividerRoot}} />
                    <MenuItem onClick={showCreateAccount.bind(this)} classes={{root:classes.menuItem}}>
                        <AddIcon className={classes.icon} /> 创建账户
                    </MenuItem>
                    <MenuItem onClick={showImportAccount.bind(this)} classes={{root:classes.menuItem}}>
                        <VerticalAlignBottomIcon className={classes.icon} /> 导入账户
                    </MenuItem>
                    <Divider classes={{root:classes.dividerRoot}} />
                    <MenuItem onClick={showSetting.bind(this)} classes={{root:classes.menuItem}}>
                        <SettingsIcon  className={classes.icon} /> 设置
                    </MenuItem>
                </Menu>
                { <AccountDetail open={accountDetail} closeCallback={handleCloseDetail.bind(this)} />}
                { <ImportAccount open={importAccount} closeCallback={handleCloseImport.bind(this)} />}
                { <CreateAccount open={createAccount} closeCallback={handleCloseCreate.bind(this)} />}
            </>
        )
    }

    return (
        <div className={classes.root}>
            {wallet ? showAccountBtn() : ""}
        </div>
    )
}

export default withRouter(AccountBtn)
